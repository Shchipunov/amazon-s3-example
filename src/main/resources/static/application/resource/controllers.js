angular.module("resourceApplication")
	.controller("resourceController", function($scope, $http, $uibModal) {
		$scope.getResources = function() {
			$http.get("/resource").success(function(data) {
				$scope.resources = data;
			});
		}
		$scope.showUploadResourceForm = function() {
			var modalInstance = $uibModal.open({
				templateUrl : 'uploadResourceForm.html',
				controller : 'uploadResourceController',
				resolve : {
					resources : function() {
						return $scope.resources;
					}
				}
			});
			modalInstance.result.then(function(resourceInformation) {
				$scope.resources.push(resourceInformation);
			});
		}
		$scope.getResources();
		$scope.$watchCollection("resources", function() {
			if (angular.isDefined($scope.resources)) {
				$scope.totalResources = $scope.resources.length;
			}
		});
	})
	.controller("uploadResourceController", function($scope, $http, $uibModalInstance, resources) {
		$scope.resources = resources;
		$scope.upload = function() {
			$('#upload-resource-spinner').show();
			$('#upload-resource-submit').button('loading');
			var formData = new FormData(document.getElementById('upload-resource-form'));
			$http.post("/upload", formData, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).success(function(data) {
				$uibModalInstance.close(data);
			}).error(function(data) {
				$scope.error = data;
			});
		}
		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	});