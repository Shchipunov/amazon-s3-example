package com.shchipunov.workshop.controller;

import static java.util.Objects.requireNonNull;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shchipunov.workshop.component.ResourceStorage;
import com.shchipunov.workshop.component.ResourceWrapper;
import com.shchipunov.workshop.domain.ResourceInformation;
import com.shchipunov.workshop.service.ResourceService;

/**
 * Allows to show all uploaded resources and download them
 * 
 * @author Shchipunov Stanislav
 */
@RestController
public class DownloadResourceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DownloadResourceController.class);

	private final ResourceService resourceService;

	@Autowired
	public DownloadResourceController(ResourceService resourceService, ResourceStorage resourceStorage) {
		this.resourceService = requireNonNull(resourceService);
	}

	/**
	 * Renders all uploaded resources
	 * 
	 * @return
	 */
	@RequestMapping(method = { GET }, value = "/resources")
	public List<ResourceInformation> findResources() {
		return resourceService.findAll();
	}

	/**
	 * Allows to download a resource for the given URL
	 * 
	 * @param date
	 * @param uuid
	 * @param resourceName
	 * @return
	 */
	@RequestMapping(method = { GET }, value = "/download/{date}/{uuid}/{resourceName:.+}")
	public ResponseEntity<Resource> downloadResource(
			@PathVariable String date, 
			@PathVariable String uuid,
			@PathVariable String resourceName) {
		LOGGER.trace("Date: {}", date);
		LOGGER.trace("UUID: {}", uuid);
		LOGGER.trace("Resource name: {}", resourceName);
		Optional<ResourceWrapper> resourceWrapperOptional = resourceService
				.findOne(date + "/" + uuid + "/" + resourceName);
		return resourceWrapperOptional
				.map(resourceWrapper -> new ResponseEntity<Resource>(
						new InputStreamResource(resourceWrapper.getInputStream()), HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
}
