package com.shchipunov.workshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Shchipunov Stanislav
 */
@Controller
public class IndexPageController {

	/**
	 * Shows index page
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String showIndexPage() {
		return "indexPage";
	}
}
