package com.shchipunov.workshop.controller;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.shchipunov.workshop.component.MultipartFileWrapperImplementation;
import com.shchipunov.workshop.domain.ResourceInformation;
import com.shchipunov.workshop.exception.UploadResourceException;
import com.shchipunov.workshop.service.ResourceService;

/**
 * Allows to upload resources
 * 
 * @author Shchipunov Stanislav
 */
@RestController
@RequestMapping("/upload")
public class UploadResourceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UploadResourceController.class);

	private final ResourceService resourceService;

	@Autowired
	public UploadResourceController(ResourceService resourceService) {
		this.resourceService = Objects.requireNonNull(resourceService);
	}
	
	/**
	 * Takes {@link ResourceInformation} and an uploaded file and stores them
	 * 
	 * @param resourceInformation
	 * @param resource
	 * @param uriComponentsBuilder
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ResourceInformation> uploadResource(ResourceInformation resourceInformation,
			@RequestPart MultipartFile resource, UriComponentsBuilder uriComponentsBuilder) {
		LOGGER.debug("Starts to process a request...");
		resourceInformation = resourceService.save(resourceInformation,
				new MultipartFileWrapperImplementation(resource));
		HttpHeaders httpHeaders = new HttpHeaders();
		URI location = uriComponentsBuilder
				.path("/download/")
				.path(resourceInformation.getPathToResource())
				.build()
				.toUri();
		httpHeaders.setLocation(location);
		LOGGER.debug("Resource was uploaded successfully! ID: {}, path: {}", resourceInformation.getId(),
				resourceInformation.getPathToResource());
		return new ResponseEntity<ResourceInformation>(resourceInformation, httpHeaders, HttpStatus.CREATED);
	}

	/**
	 * Handles {@link UploadResourceException} exception
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(UploadResourceException.class)
	public ResponseEntity<String> handleUploadResourceException(UploadResourceException exception) {
		Optional.ofNullable(exception.getCause()).ifPresent(cause -> LOGGER.warn(cause.getMessage()));
		return new ResponseEntity<String>(exception.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
