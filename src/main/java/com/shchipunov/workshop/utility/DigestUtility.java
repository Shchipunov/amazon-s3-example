package com.shchipunov.workshop.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Presents an utility that allows to get hash code of resource
 * 
 * @author Shchipunov Stanislav
 */
public class DigestUtility {

	private DigestUtility() {

	}

	public static String hash(File file, String algorithm) {
		try {
			return hash(Files.readAllBytes(file.toPath()), algorithm);
		} catch (IOException exception) {
			throw new IllegalArgumentException(exception);
		}
	}

	public static String hash(byte[] bytes, String algorithm) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
			messageDigest.update(bytes);
			byte[] digest = messageDigest.digest();

			StringBuilder hashCode = new StringBuilder();
			for (int i = 0; i < digest.length; i++) {
				if ((0xff & digest[i]) < 0x10) {
					hashCode.append("0" + Integer.toHexString((0xFF & digest[i])));
				} else {
					hashCode.append(Integer.toHexString(0xFF & digest[i]));
				}
			}
			return hashCode.toString();
		} catch (NoSuchAlgorithmException exception) {
			throw new IllegalArgumentException(exception);
		}
	}
}
