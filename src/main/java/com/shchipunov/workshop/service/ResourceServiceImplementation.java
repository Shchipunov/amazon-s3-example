package com.shchipunov.workshop.service;

import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shchipunov.workshop.component.PathGenerator;
import com.shchipunov.workshop.component.ResourceStorage;
import com.shchipunov.workshop.component.ResourceWrapper;
import com.shchipunov.workshop.domain.ResourceInformation;
import com.shchipunov.workshop.exception.UploadResourceException;
import com.shchipunov.workshop.repository.ResourceInformationRepository;

/**
 * Presents a class that handles resources. It delegates persistence operations
 * to {@link ResourceInformationRepository} and {@link ResourceStorage}
 * 
 * @author Shchipunov Stanislav
 */
@Service
public class ResourceServiceImplementation implements ResourceService {

	/**
	 * Root folder where all files will be stored. It can be be specified in
	 * .properties file.
	 */
	private static final String DEFAULT_ROOT_FOLDER = "...";

	private final PathGenerator pathGenerator;
	private final ResourceInformationRepository resourceInformationRepository;
	private final ResourceStorage resourceStorage;

	@Autowired
	public ResourceServiceImplementation(PathGenerator pathGenerator,
			ResourceInformationRepository resourceInformationRepository, ResourceStorage resourceStorage) {
		this.pathGenerator = requireNonNull(pathGenerator);
		this.resourceInformationRepository = requireNonNull(resourceInformationRepository);
		this.resourceStorage = requireNonNull(resourceStorage);

	}

	@Override
	public Optional<ResourceWrapper> findOne(String pathToResource) {
		return resourceStorage.findOne(DEFAULT_ROOT_FOLDER, pathToResource);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ResourceInformation> findAll() {
		return resourceInformationRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = UploadResourceException.class)
	public ResourceInformation save(ResourceInformation resourceInformation, ResourceWrapper resourceWrapper)
			throws UploadResourceException {
		String pathToResource = pathGenerator.next(resourceWrapper.getOriginalName());

		resourceInformation.setPathToResource(pathToResource);
		resourceInformation.setOriginalName(resourceWrapper.getOriginalName());
		resourceInformation.setContentLength(resourceWrapper.getContentLength());

		resourceInformation = resourceInformationRepository.save(resourceInformation);
		resourceStorage.save(DEFAULT_ROOT_FOLDER, pathToResource, resourceWrapper);

		return resourceInformation;
	}

}
