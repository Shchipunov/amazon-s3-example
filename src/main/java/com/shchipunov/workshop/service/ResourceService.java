package com.shchipunov.workshop.service;

import java.util.List;
import java.util.Optional;

import com.shchipunov.workshop.component.ResourceWrapper;
import com.shchipunov.workshop.domain.ResourceInformation;
import com.shchipunov.workshop.exception.UploadResourceException;

public interface ResourceService {

	Optional<ResourceWrapper> findOne(String pathToResource);

	List<ResourceInformation> findAll();

	ResourceInformation save(ResourceInformation resourceInformation, ResourceWrapper resourceWrapper)
			throws UploadResourceException;
}
