package com.shchipunov.workshop.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.EqualsAndHashCode;

/**
 * Presents a class that holds resource information
 * 
 * @author Shchipunov Stanislav
 */
@Entity
@EqualsAndHashCode(of = { "pathToResource" })
public class ResourceInformation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String pathToResource;
	private String originalName;
	private long contentLength;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	public ResourceInformation() {
		this.creationDate = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPathToResource() {
		return pathToResource;
	}

	public void setPathToResource(String pathToResource) {
		this.pathToResource = pathToResource;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

}
