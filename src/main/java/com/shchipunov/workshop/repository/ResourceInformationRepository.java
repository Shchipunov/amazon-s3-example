package com.shchipunov.workshop.repository;

import java.util.List;

import com.shchipunov.workshop.domain.ResourceInformation;

/**
 * Repository to access {@link ResourceInformation} instances
 * 
 * @author Shchipunov Stanislav
 */
public interface ResourceInformationRepository {

	ResourceInformation findOne(Long id);

	List<ResourceInformation> findAll();

	ResourceInformation save(ResourceInformation resourceInformation);

	void delete(ResourceInformation resourceInformation);
}
