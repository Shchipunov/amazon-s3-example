package com.shchipunov.workshop.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shchipunov.workshop.domain.ResourceInformation;

/**
 * Presents an implementation of {@link ContactRepository}
 * 
 * @author Shchipunov Stanislav
 */
@Repository
@Transactional
public class ResourceInformationRepositoryImplementation implements ResourceInformationRepository {

	private static final String FIND_BY_ID_STATEMENT = "SELECT * FROM resource_information WHERE id = ?";
	private static final String FIND_ALL_STATEMENT = "SELECT * FROM resource_information";
	private static final String SAVE_STATEMENT = "INSERT INTO resource_information(path_to_resource, original_name, "
			+ "content_length, creation_date) VALUES(?, ?, ?, ?)";
	private static final String DELETE_STATEMENT = "DELETE FROM resource_information WHERE id = ?";

	private final JdbcOperations jdbcOperations;

	@Autowired
	public ResourceInformationRepositoryImplementation(JdbcOperations jdbcOperations) {
		this.jdbcOperations = Objects.requireNonNull(jdbcOperations);
	}

	/**
	 * Finds resource information
	 */
	@Override
	@Transactional(readOnly = true)
	public ResourceInformation findOne(Long id) {
		return jdbcOperations.queryForObject(FIND_BY_ID_STATEMENT, this::mapResourceInformation, id);
	}

	/**
	 * Finds all resource information
	 */
	@Override
	@Transactional(readOnly = true)
	public List<ResourceInformation> findAll() {
		return jdbcOperations.query(FIND_ALL_STATEMENT, this::mapResourceInformation);
	}

	/**
	 * Saves resource information
	 */
	@Override
	public ResourceInformation save(ResourceInformation resourceInformation) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcOperations.update(connection -> {
			PreparedStatement preparedStatement = connection.prepareStatement(SAVE_STATEMENT, new String[] { "id" });
			preparedStatement.setString(1, resourceInformation.getPathToResource());
			preparedStatement.setString(2, resourceInformation.getOriginalName());
			preparedStatement.setLong(3, resourceInformation.getContentLength());
			preparedStatement.setTimestamp(4, new Timestamp(resourceInformation.getCreationDate().getTime()));
			return preparedStatement;
		}, keyHolder);
		resourceInformation.setId(keyHolder.getKey().longValue());
		return resourceInformation;
	}

	/**
	 * Deletes resource information
	 */
	@Override
	public void delete(ResourceInformation contact) {
		jdbcOperations.update(DELETE_STATEMENT, contact.getId());
	}

	private ResourceInformation mapResourceInformation(ResultSet resultSet, int rowNumber) throws SQLException {
		ResourceInformation resourceInformation = new ResourceInformation();
		resourceInformation.setId(resultSet.getLong("id"));
		resourceInformation.setPathToResource(resultSet.getString("pathToResource"));
		resourceInformation.setOriginalName(resultSet.getString("originalName"));
		resourceInformation.setContentLength(resultSet.getLong("contentLength"));
		resourceInformation.setCreationDate(resultSet.getDate("creationDate"));
		return resourceInformation;
	}
}
