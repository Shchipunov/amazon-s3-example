package com.shchipunov.workshop.component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Objects;

import org.springframework.web.multipart.MultipartFile;

import com.shchipunov.workshop.utility.DigestUtility;

import lombok.EqualsAndHashCode;

/**
 * Presents an implementation of {@link ResourceWrapper}
 * 
 * @author Shchipunov Stanislav
 */
@EqualsAndHashCode(of = { "multipartFile" })
public class MultipartFileWrapperImplementation implements ResourceWrapper {

	private final MultipartFile multipartFile;
	private final long lastModified;

	/**
	 * Creates a new {@link MultipartFileWrapperImplementation} that wraps the
	 * given {@link MultipartFile}
	 * 
	 * @param multipartFile
	 *            must not be {@literal null}.
	 */
	public MultipartFileWrapperImplementation(MultipartFile multipartFile) {
		this.multipartFile = Objects.requireNonNull(multipartFile);
		this.lastModified = new Date().getTime();
	}

	@Override
	public String getOriginalName() {
		return multipartFile.getOriginalFilename();
	}

	@Override
	public InputStream getInputStream() {
		try {
			return multipartFile.getInputStream();
		} catch (IOException exception) {
			throw new IllegalArgumentException(exception);
		}
	}

	@Override
	public long getContentLength() {
		return multipartFile.getSize();
	}

	@Override
	public String getContentType() {
		return multipartFile.getContentType();
	}

	@Override
	public String getETag() {
		try {
			return "\"" + DigestUtility.hash(multipartFile.getBytes(), "SHA-512") + "\"";
		} catch (IOException exception) {
			return null;
		}
	}

	@Override
	public long getLastModified() {
		return lastModified;
	}
}
