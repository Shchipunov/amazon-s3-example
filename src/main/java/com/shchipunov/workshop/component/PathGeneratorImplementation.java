package com.shchipunov.workshop.component;

import org.springframework.stereotype.Component;

/**
 * Presents an implementation of {@link PathGenerator}
 * 
 * @author Shchipunov Stanislav
 */
@Component
public class PathGeneratorImplementation implements PathGenerator {

}
