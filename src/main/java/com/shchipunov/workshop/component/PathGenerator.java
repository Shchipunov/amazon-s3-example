package com.shchipunov.workshop.component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * Presents an interface with default methods that allow to generate a path to a
 * resource where it would be stored
 * 
 * @author Shchipunov Stanislav
 */
public interface PathGenerator {

	default String next() {
		String firstPartOfPath = DateTimeFormatter.BASIC_ISO_DATE.format(LocalDate.now());
		String secondPartOfPath = UUID.randomUUID().toString();
		return new StringBuilder()
				.append(firstPartOfPath)
				.append("/")
				.append(secondPartOfPath)
				.toString();
	}
	
	default String next(String finalPartOfPath) {
		return new StringBuilder(next())
				.append("/")
				.append(finalPartOfPath)
				.toString();
	}
}
