package com.shchipunov.workshop.component;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.shchipunov.workshop.exception.UploadResourceException;

/**
 * Presents an implementation of {@link ResourceStorage}
 * 
 * @author Shchipunov Stanislav
 */
@Component
public class S3ObjectStorageImplementation implements ResourceStorage {

	private static final Logger LOGGER = LoggerFactory.getLogger(S3ObjectStorageImplementation.class);

	/**
	 * Credentials to access to S3. They can be be specified in .properties
	 * file.
	 */
	private final BasicAWSCredentials credentials = new BasicAWSCredentials("...", "...");
	private final AmazonS3 client = new AmazonS3Client(credentials);
	private final TransferManager transferManager = new TransferManager(credentials);

	/**
	 * Finds a resource
	 */
	@Override
	public Optional<ResourceWrapper> findOne(String rootFolder, String pathToResource) {
		LOGGER.debug("Root folder: {}", rootFolder);
		LOGGER.debug("Path to resource: {}", pathToResource);

		S3Object object = client.getObject(new GetObjectRequest(rootFolder, pathToResource));
		ResourceWrapper resourceWrapper = new S3ObjectWrapperImplementation(object);
		return Optional.of(resourceWrapper);
	}

	/**
	 * Saves a resource
	 */
	@Override
	public void save(String rootFolder, String pathToResource, ResourceWrapper resourceWrapper)
			throws UploadResourceException {
		LOGGER.debug("Root folder: {}", rootFolder);
		LOGGER.debug("Path to resource: {}", pathToResource);
		LOGGER.debug("Name: {}", resourceWrapper.getOriginalName());
		LOGGER.debug("Content type: {}", resourceWrapper.getContentType());
		LOGGER.debug("Content length: {}", resourceWrapper.getContentLength());

		ObjectMetadata objectMetadata = new ObjectMetadata();
		objectMetadata.setContentType(resourceWrapper.getContentType());
		objectMetadata.setContentLength(resourceWrapper.getContentLength());
		objectMetadata.setLastModified(new Date(resourceWrapper.getLastModified()));
		objectMetadata.setHeader(HttpHeaders.ETAG, resourceWrapper.getETag());

		AccessControlList accessControlList = new AccessControlList();
		accessControlList.grantPermission(GroupGrantee.AllUsers, Permission.Read);

		PutObjectRequest putObjectRequest = new PutObjectRequest(rootFolder, pathToResource,
				resourceWrapper.getInputStream(), objectMetadata).withAccessControlList(accessControlList);
		try {
			LOGGER.debug("Starts to upload resource...");
			Upload upload = transferManager.upload(putObjectRequest);
			upload.waitForCompletion();
			LOGGER.debug("Resource was uploaded successfully!");
		} catch (AmazonClientException | InterruptedException exception) {
			throw new UploadResourceException(
					String.format("Unable to upload resource: %s", resourceWrapper.getOriginalName()), exception);
		}
		transferManager.shutdownNow();
	}

	/**
	 * Deletes a resource
	 */
	@Override
	public void delete(String rootFolder, String pathToResource) {
		LOGGER.debug("Root folder: {}", rootFolder);
		LOGGER.debug("Path to resource: {}", pathToResource);
		LOGGER.debug("Starts to delete resource...");

		client.deleteObject(new DeleteObjectRequest(rootFolder, pathToResource));

		LOGGER.debug("Resource was deleted successfully!");
	}
}
