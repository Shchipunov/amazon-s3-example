package com.shchipunov.workshop.component;

import java.util.Optional;

import com.shchipunov.workshop.exception.UploadResourceException;

/**
 * Presents an interface of the storage abstraction
 * 
 * @author Shchipunov Stanislav
 */
public interface ResourceStorage {

	Optional<ResourceWrapper> findOne(String rootFolder, String pathToResource);

	void save(String rootFolder, String pathToResource, ResourceWrapper resourceWrapper) throws UploadResourceException;

	void delete(String rootFolder, String pathToResource);
}