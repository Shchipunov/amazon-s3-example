package com.shchipunov.workshop.component;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Objects;

import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3Object;

/**
 * Presents an implementation of {@link ResourceWrapper}
 * 
 * @author Shchipunov Stanislav
 */
public class S3ObjectWrapperImplementation implements ResourceWrapper {

	private final S3Object object;

	/**
	 * Creates a new {@link MultipartFileWrapperImplementation} that wraps the
	 * given {@link MultipartFile}
	 * 
	 * @param object
	 *            must not be {@literal null}.
	 */
	public S3ObjectWrapperImplementation(S3Object object) {
		this.object = Objects.requireNonNull(object);
	}

	@Override
	public String getOriginalName() {
		return object.getKey();
	}

	@Override
	public String getContentType() {
		return object.getObjectMetadata().getContentType();
	}

	@Override
	public String getETag() {
		return object.getObjectMetadata().getETag();
	}

	@Override
	public InputStream getInputStream() {
		return new BufferedInputStream(object.getObjectContent());
	}

	@Override
	public long getContentLength() {
		return object.getObjectMetadata().getContentLength();
	}

	@Override
	public long getLastModified() {
		return object.getObjectMetadata().getLastModified().getTime();
	}
}
