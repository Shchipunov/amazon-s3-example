package com.shchipunov.workshop.component;

import java.io.InputStream;

/**
 * Presents an interface of the resource abstraction
 * 
 * @author Shchipunov Stanislav
 */
public interface ResourceWrapper {

	String getOriginalName();

	String getContentType();

	String getETag();

	InputStream getInputStream();

	long getContentLength();

	long getLastModified();
}
