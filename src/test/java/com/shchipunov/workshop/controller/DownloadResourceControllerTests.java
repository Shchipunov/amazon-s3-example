package com.shchipunov.workshop.controller;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.shchipunov.workshop.component.ResourceStorage;
import com.shchipunov.workshop.component.ResourceWrapper;
import com.shchipunov.workshop.service.ResourceService;

/**
 * @author Shchipunov Stanislav
 */
@RunWith(SpringRunner.class)
@WebMvcTest(DownloadResourceController.class)
public class DownloadResourceControllerTests {

	private static final String PATH_TO_RESOURCE = "20160821/fcb1edde-bbb8-43fa-9cd9-ba18314537fb/test.txt";
	
	private static final String RESOURCE_CONTENT = "";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ResourceStorage resourceStorage;
	@MockBean
	private ResourceService resourceService;

	@Test
	public void shouldShow200() throws Exception {
		given(resourceService.findOne(PATH_TO_RESOURCE)).willReturn(resourceWrapper());

		mockMvc
				.perform(get("/download/" + PATH_TO_RESOURCE))
				.andExpect(status().isOk());
	}

	private Optional<ResourceWrapper> resourceWrapper() {
		ResourceWrapper resourceWrapper = new ResourceWrapper() {

			@Override
			public String getOriginalName() {
				return null;
			}

			@Override
			public long getLastModified() {
				return new Date().getTime();
			}

			@Override
			public InputStream getInputStream() {
				return new ByteArrayInputStream(RESOURCE_CONTENT.getBytes());
			}

			@Override
			public String getETag() {
				return null;
			}

			@Override
			public String getContentType() {
				return "text/plain";
			}

			@Override
			public long getContentLength() {
				return 0;
			}
		};
		return Optional.of(resourceWrapper);
	}
}
