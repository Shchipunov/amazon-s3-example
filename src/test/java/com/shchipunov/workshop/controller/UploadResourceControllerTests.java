package com.shchipunov.workshop.controller;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.shchipunov.workshop.component.MultipartFileWrapperImplementation;
import com.shchipunov.workshop.domain.ResourceInformation;
import com.shchipunov.workshop.service.ResourceService;

/**
 * @author Shchipunov Stanislav
 */
@RunWith(SpringRunner.class)
@WebMvcTest(UploadResourceController.class)
public class UploadResourceControllerTests {

	private static final String PATH_TO_RESOURCE = "20160821/fcb1edde-bbb8-43fa-9cd9-ba18314537fb/test.txt";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ResourceService resourceService;

	@Test
	public void shouldShow201() throws Exception {
		ResourceInformation resourceInformation = resourceInformation();
		resourceInformation.setPathToResource(PATH_TO_RESOURCE);
		MockMultipartFile resource = new MockMultipartFile("resource", "text/plain", "test.txt",
				new String().getBytes());

		given(resourceService.save(resourceInformation(), new MultipartFileWrapperImplementation(resource)))
				.willReturn(resourceInformation);

		mockMvc
				.perform(fileUpload("/upload").file(resource))
				.andExpect(status().isCreated())
				.andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/download/" + PATH_TO_RESOURCE));
	}

	private ResourceInformation resourceInformation() {
		return new ResourceInformation();
	}
}
