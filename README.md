# Amazon S3 example #

This example demonstrates using of AWS SDK for Java to upload / download resources.

### Project description ###
The project uses:

* AWS SDK for Java
* Spring Boot
* Spring (MVC)
* Spring JDBC template